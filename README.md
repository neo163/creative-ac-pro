# Creative AC Pro
Creative AC Pro Project<br/>
创意AI官网：https://creative.chat/

## Install Python:
https://www.python.org/downloads/release/python-3913/ <br/>
Python 3.9.13

## Install require packages

```
pip install -r requirements.txt
```

## Run Project

Go to the location of Project

```
python main.py
```
Or
```
uvicorn main:app --reload --port 1888
```

## 项目效果预览图

<img src="https://gitee.com/neo163/creative-ac-pro-web/raw/master/src/assets/project_effects/chat1.png" alt="Creative AC Pro Web" style="zoom:100%;" />

## AI系统配套前端项目

Creative AC Pro Web: <a href="https://gitee.com/neo163/creative-ac-pro-web" target="_blank">https://gitee.com/neo163/creative-ac-pro-web</a>

Creative AC Pro Web 目前功能有：

1. LLM聊天对话功能
2. Markdown格式展示LLM聊天对话
3. LLM上下文记忆
4. 流式输出LLM回复内容
5. LLM回复过程中，禁止用户再次发送问题
6. RAG功能
