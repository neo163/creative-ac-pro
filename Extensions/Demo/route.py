from fastapi import APIRouter, Form
from pydantic import BaseModel

router = APIRouter()


class RequestData(BaseModel):
    text: str


@router.get("/demo")
async def root():
    return {"code": 200, "result": "CreativeAC!"}
